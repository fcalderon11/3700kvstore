#!/usr/bin/env python

import sys, socket, select, time, json, random

# print("STARTING UP<><<>?><><><><><><><><><><><><><>")
# Your ID number
my_id = sys.argv[1]

# The ID numbers of all the other replicas
replica_ids = sys.argv[2:]

key_store = {}
# List of dictionaries { term, index, command }
logs = []
state_machine = []
# Leader, Follower, Candidate
states = ['L', 'F', 'C']
state = states[1]
current_term = 0
# timeout in seconds
timeout = 0.2
leader_timeout = 0.1
follower_timeout = 0.3
send_hb_every = 0.1
election_timeout = 0.5
did_election_timeout = False
wait_time_range = [.150, .300]
time_election_started = 0
timed_out = False
leader_id = None
index_acked = {}
oks_received = 0
next_indices = {}


# whether we are in an election
in_election = False

# whether we voted
voted = False

# number of votes received
votes = 0

# Connect to the network. All messages to/from other replicas and clients will
# occur over this socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
sock.connect(my_id)

last = 0

leader_last_rpc_at = time.time()


def handle_request_vote(_sock, _msg):
    global in_election
    global leader_last_rpc_at
    global voted
    global votes

    leader_last_rpc_at = time.time()

    # print("GOT RV", msg, current_term, my_id)
    leader_id = None
    if not in_election:
        in_election = True
        time_election_started = time.time()
    if "voteGranted" not in _msg:
        if _msg['term'] < current_term:
            _sock.send(json.dumps({'type': 'RV', "src": my_id, "term": current_term, "voteGranted": False, "dst": _msg['src'], "leader": "FFFF"})  + '\n')
        elif _msg['term'] > current_term and not voted:
            lastLogIndex = None
            lastLogTerm = None
            if logs:
                lastLogIndex = len(logs) - 1
                lastLogTerm = logs[-1]['term']

            if not lastLogIndex or lastLogIndex < _msg['lastLogIndex']:
                # print("GRANTED VOTE", "<<<<<<<<<<<<<<<<<<<<<<<<")
                _sock.send(json.dumps({'type': 'RV', "src": my_id, "term": current_term, "voteGranted": True, "dst": _msg['src'], "leader": "FFFF"}) + '\n')
                voted = True
                votes -= 1
                state = states[1]
            elif lastLogIndex > _msg['lastLogIndex']:
                _sock.send(json.dumps({'type': 'RV', "src": my_id, "term": current_term, "voteGranted": False, "dst": _msg['src'], "leader": "FFFF"}) + '\n')
            # elif lastLogTerm
    else:
        if _msg['voteGranted']:
            votes += 1
            if votes > len(replica_ids) / 2:
                # I won the election

                for r in replica_ids:
                    next_indices[r] = len(logs)
                leader_id = my_id
                send_append_entry(_sock)
                logs.append({"term": current_term, "msg": None})

                in_election = False


def send_append_entry(_socket, entries=None, dst=None):
    if not entries:
        entries = []
    if not dst:
        dst = "FFFF"
    global replica_ids
    if logs:
        hb_msg = {"type": "AE", "src": my_id, "dst": dst, "term": current_term, "leader": my_id, "prevLogIndex": len(logs) - 1, "prevLogTerm": logs[-1]["term"], "entries": entries, "leaderCommit": ''}
    else:
        hb_msg = {"type": "AE", "src": my_id, "dst": dst, "term": current_term, "leader": my_id, "prevLogIndex": None, "prevLogTerm": None, "entries": entries, "leaderCommit": ''}
    # print("SENDING", hb_msg)
    _socket.send(json.dumps(hb_msg) + '\n')


def start_election(_socket):
    global current_term
    global votes
    global in_election
    global voted
    global time_election_started
    global election_timeout
    time_election_started = time.time()
    election_timeout = random.uniform(wait_time_range[0], wait_time_range[1])

    # print "starting election"

    current_term += 1
    in_election = True
    rv_msg = {}
    # voted = True
    votes += 1
    if logs:
        rv_msg = {"type": 'RV', "term": current_term, "src": my_id, "dst":"FFFF", "lastLogIndex": len(logs) - 1, "lastLogTerm": logs[-1]["term"], "leader": "FFFF"}
    else:
        rv_msg = {"type": 'RV', "term": current_term, "src": my_id, "dst":"FFFF", "lastLogIndex": None, "lastLogTerm": None, "leader": "FFFF"}
    _socket.send(json.dumps(rv_msg) + '\n')


def commit(index):
    # print(logs[-1])
    msg = logs[index]['msg']
    if not msg:
        return
    if msg['type'] == 'get':
        key = msg['key']
        value = ''
        if key in key_store:
            value = key_store[key]
        sock.send(json.dumps({'src': my_id, 'dst': msg['src'], 'leader': leader_id, 'type': 'ok', 'MID': msg['MID'], 'value': value}) + '\n')
    elif msg['type'] == 'put':
        key_store[msg['key']] = msg['value']
        sock.send(json.dumps({'src': my_id, 'dst': msg['src'], 'leader': leader_id, 'type': 'ok', 'MID': msg['MID']}) + '\n')


while True:
    if in_election and time.time() - time_election_started > election_timeout:
        did_election_timeout = True
        start_election(sock)
        # print("ELECTION TIMEOUT")
    if leader_id and leader_id == my_id:
        timeout = leader_timeout
    else:
        timeout = follower_timeout

    ready = select.select([sock], [], [], timeout)[0]

    if sock in ready:
        try:
            raw = sock.recv(32768)
        except timeout:
            if leader_id and leader_id == my_id:
                send_append_entry(sock)
                logs.append({"term": current_term, "msg": None})
                continue
            else:
                state = states[2]
                sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                sock.connect(my_id)
                start_election(sock)
                leader_id = None
                continue

        # we may receive multiple messages during a single recv(); \n denotes the break between messages
        for msg_raw in raw.split('\n'):
            if len(msg_raw) == 0: continue
            # print(">>>>>>>>>>>><><><><><>",msg_raw, my_id)
            msg = json.loads(msg_raw)

            if leader_id and msg['type'] in ['put', 'get']:
                if in_election:
                    sock.send(json.dumps({'src': my_id, 'dst': msg['src'], 'leader': 'FFFF','type': 'fail', 'MID': msg['MID']}) + '\n')
                    continue
                if not leader_id == my_id:
                    sock.send(json.dumps({'src':my_id, 'dst': msg['src'], 'leader': leader_id, 'type':'redirect', 'MID': msg['MID']}) + '\n')
                    continue

                logs.append({"term": current_term, "msg": msg})
                if msg['type'] == 'get':
                    key = msg['key']
                    value = ''
                    if key in key_store:
                        value = key_store[key]
                    sock.send(json.dumps({'src': my_id, 'dst': msg['src'], 'leader': leader_id, 'type': 'ok', 'MID': msg['MID'], 'value': value}) + '\n')
                elif msg['type'] == 'put':
                    key_store[msg['key']] = msg['value']
                    sock.send(json.dumps({'src': my_id, 'dst': msg['src'], 'leader': leader_id, 'type': 'ok', 'MID': msg['MID']}) + '\n')


                # send_append_entry(sock, [{"term": current_term, "msg": msg}])

            else:
                # message must be an RPC
                if msg['type'] == 'RV':
                    # handle_request_vote(sock, msg)
                    leader_last_rpc_at = time.time()

                    # print("GOT RV", msg, current_term, my_id)
                    leader_id = None
                    if not in_election:
                        in_election = True
                        time_election_started = time.time()
                    if "voteGranted" not in msg:
                        if msg['term'] < current_term:
                            sock.send(json.dumps({'type': 'RV', "src": my_id, "term": current_term, "voteGranted": False, "dst": msg['src'], "leader": "FFFF"})  + '\n')
                        elif msg['term'] > current_term and not voted:
                            lastLogIndex = None
                            lastLogTerm = None
                            if logs:
                                lastLogIndex = len(logs) - 1
                                lastLogTerm = logs[-1]['term']

                            if not lastLogIndex or lastLogIndex < msg['lastLogIndex']:
                                # print("GRANTED VOTE", "<<<<<<<<<<<<<<<<<<<<<<<<")
                                sock.send(json.dumps({'type': 'RV', "src": my_id, "term": current_term, "voteGranted": True, "dst": msg['src'], "leader": "FFFF"}) + '\n')
                                voted = True
                                votes -= 1
                                state = states[1]
                            elif lastLogIndex > msg['lastLogIndex']:
                                sock.send(json.dumps({'type': 'RV', "src": my_id, "term": current_term, "voteGranted": False, "dst": msg['src'], "leader": "FFFF"}) + '\n')
                            # elif lastLogTerm
                    else:
                        if msg['voteGranted']:
                            votes += 1
                            if votes > len(replica_ids) / 2:
                                # I won the election

                                for r in replica_ids:
                                    next_indices[r] = len(logs)
                                leader_id = my_id
                                send_append_entry(sock)
                                logs.append({"term": current_term, "msg": None})

                                in_election = False
                if leader_id and leader_id == my_id:
                    if msg["type"] == 'ack':
                        if msg['success']:
                            # print("SUCCESSS")
                            if msg["c_index"] not in index_acked:
                                index_acked[msg["c_index"]] = 0

                            index_acked[msg["c_index"]] += 1
                            next_indices[msg['src']] += 1
                            if index_acked[msg["c_index"]] > (len(replica_ids) / 2) + 1:
                                # print("REAAAACHEDD <><><><><><><><><<>")
                                commit(msg["c_index"])
                                oks_received = 0
                        else:
                            # print("NOOT SUCCESSS", msg, current_term)
                            if current_term < msg['term']:
                                leader_id = msg['leader']
                            else:
                                next_indices[msg['src']] -= 1
                                send_append_entry(sock, logs[next_indices[msg['src']]:], msg['src'])

                if msg["type"] == 'AE' and not leader_id == my_id:
                    # print("GOT AE", msg)
                    in_election = False
                    term = 1
                    if logs:
                        term = logs[-1]['term']
                    leader_id = msg['leader']
                    if logs and logs[-1]['term'] > msg['term']:
                        print("GHIS SS ")
                        sock.send(json.dumps({"src": my_id, 'dst': msg['src'], 'type': 'ack', 'success': False, 'leader': leader_id, 'term': term}) + '\n')
                        continue
                    # print msg
                    pLi = msg['prevLogIndex']
                    pLt = msg['prevLogTerm']
                    # print pLi
                    # print pLt

                    if (logs and pLi and pLt) and (pLi >= len(logs) or not logs[pLi]['term'] == pLt):
                        print(pLi, pLt, logs, len(logs), my_id)
                        print("<><><><><><>< FCSSDUP ")
                        sock.send(json.dumps({"src": my_id, 'dst': msg['src'], 'type': 'ack', 'success': False, 'leader': leader_id, 'term': term}) + '\n')
                        continue

                    count = 0
                    if not msg['entries'] or len(msg['entries']) == 0:
                        logs.append({'term': msg['term'], 'msg': None})
                    for entry in msg['entries']:
                        count += 1
                        if pLi + count < len(logs):
                            logs[pLi + count] = entry
                        else:
                            logs.append(entry)
                    if pLi:
                        sock.send(json.dumps({"src": my_id, 'dst': msg['src'], 'type': 'ack', 'success': True, 'leader': leader_id, 'term': term, "c_index": pLi + 1}) + '\n')
                    else:
                        sock.send(json.dumps({"src": my_id, 'dst': msg['src'], 'type': 'ack', 'success': True, 'leader': leader_id, 'term': term, "c_index": 1}) + '\n')
            if msg['leader'] == leader_id:
                leader_last_rpc_at = time.time()
            elif time.time() - leader_last_rpc_at > leader_timeout:
                # print("LEADER TIME OUT, STARTING ELECTION")
                state = states[2]
                start_election(sock)
                leader_id = None
                continue
                # another leader is elected
    clock = time.time()